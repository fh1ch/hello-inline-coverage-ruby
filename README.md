# hello-inline-coverage-ruby

[![pipeline status](https://gitlab.com/fh1ch/hello-inline-coverage-ruby/badges/main/pipeline.svg)](https://gitlab.com/fh1ch/hello-inline-coverage-ruby/-/commits/main)

Ruby example project to demonstrate the [GitLab Test Coverage Visualization feature](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html).

## Usage

Run:

```sh
bundle exec ruby main.rb
```

Test:

```sh
bundle exec rspec
```
